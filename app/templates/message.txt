COMPUTERS * ARE * BAD
a newsletter by j. b. crawford
--------------------------------------------------------------------------------
This post online: https://computer.rip/{{ path }}.html

{{ date }}

regarding: {{ subject }}

{{ body }}

                                                 sincerely,
                                                 j. b. crawford
                                                 me@computer.rip

================================================================================
Unsubscribe {{ address }} from this list:
    https://maleman.computer.rip/unsub/{{ address | urlencode }}/{{ token }}

Read online, tell your friends, submit to webrings, etc:
    https://computer.rip/{{ path }}.html
    https://computer.rip/

CAN-SPAM says I have to tell you if I'm a cop:
    COMPUTERS ARE BAD
    Seventh Standard LLC
    1 (877) 456-4340
    PO Box 26924
    Albuquerque, NM 87125-6924
    USA
