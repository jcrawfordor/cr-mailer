from flask import Flask
import toml
from pathlib import Path

app = Flask(__name__, instance_relative_config=True)

app.config.from_file("config.toml", load=toml.load)
app.jinja_env.lstrip_blocks = True
app.jinja_env.trim_blocks = True
app.logger.setLevel(app.config['MAILER']['loglevel'])
app.logger.info(f"Log level is {app.config['MAILER']['loglevel']}")

from app import models
models.database.init(Path(app.config['MAILER']['database']))
from app import views
from app import cli