from flask import render_template
import smtplib
import ssl
import email.utils
from app import app
import logging
from progress.bar import Bar
from pathlib import Path
from email.message import EmailMessage


def get_smtp():
    conf = app.config["SMTP"]
    context = ssl.create_default_context()
    server = smtplib.SMTP_SSL(conf["host"], conf["port"], context=context)
    server.login(conf["username"], conf["password"])
    return server


def send_validation(subscriber):
    subject = "Confirm your subscription to Computers Are Bad"
    body = render_template("confirmation.txt", address=subscriber.address, token=subscriber.token)
    smtp = get_smtp()

    msg = EmailMessage()
    msg["Subject"] = subject
    msg["From"] = '"j.b. crawford" <me@computer.rip'
    msg["To"] = subscriber.address
    msg["User-Agent"] = "cr-mailer 0.2.0 https://maleman.computer.rip"
    msg.set_content(body)

    smtp.send_message(msg)
    smtp.quit()


def send(message, contacts):
    with open(message, "r") as f:
        post_body = f.read()
    subject = Path(message).name[11:]
    email_subject = f"computers are bad: {subject}"
    date = Path(message).name[0:10]
    path = Path(message).name.replace(" ", "-")
    send_count = contacts.count()

    print(f"Subject: {subject}")
    print(f"Date: {date}")
    print(f"Path: {path}")
    print(f"Lines: {len(post_body.splitlines())}")
    print(f"Will send message to {send_count} subscribers.")
    resp = input("Type 'yes' to proceed:")
    if resp != 'yes':
        print(f"Aborting")
        return

    bar = Bar('Sending', max=send_count, width=80)
    smtp = get_smtp()
    for contact in contacts:
        body = render_template("message.txt", address=contact.address, token=contact.token, body=post_body,
                               date=date, path=path, subject=subject)
        msg = EmailMessage()
        msg["Subject"] = email_subject
        msg["From"] = '"j.b. crawford" <me@computer.rip>'
        msg["To"] = contact.address
        msg["User-Agent"] = "cr-mailer 0.3.0 https://maleman.computer.rip"
        msg.set_content(body)

        #try:
        smtp.send_message(msg)
        #except Exception as e:
        #    print(f"Send failed for {contact.address}: {e}")
        bar.next()
    smtp.quit()
    bar.finish()

    print(f"Finished sending.")


def bonus_message(message, subject, emails):
    with open(message, "r") as f:
        post_body = f.read()
    email_subject = f"computers are bad pro: {subject}"
    send_count = len(emails)

    bar = Bar('Sending', max=send_count, width=80)
    smtp = get_smtp()
    for recip in emails:
        body = render_template("bonus.txt", address=recip,
                               body=post_body, subject=subject)
        msg = EmailMessage()
        msg["Subject"] = email_subject
        msg["From"] = '"j.b. crawford" <me@computer.rip>'
        msg["To"] = recip
        msg["User-Agent"] = "cr-mailer 0.3.0 https://maleman.computer.rip"
        msg.set_content(body)

        smtp.send_message(msg)
        bar.next()
    smtp.quit()
    bar.finish()

    print(f"Finished sending.")
