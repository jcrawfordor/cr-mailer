from flask import render_template, request, send_file, make_response, escape, redirect, g, abort
from flask_hcaptcha import hCaptcha
from app import app, email
from app.models import Contact
import urllib.parse
import logging

hcaptcha = hCaptcha(app)

@app.route("/", methods=["GET", "POST"])
def subscribe():
    if request.method == "POST":
        new_email = request.form.get("email")
        if not new_email:
            return make_response(render_template("subscribe.html",
                                                 message="You have to type <em>something</em> in."))
        if "@" not in new_email or "." not in new_email:
            return make_response(render_template("subscribe.html",
                                                 message="I'm not convinced that's a <em>real</em> email address."))

        if not hcaptcha.verify():
            return make_response(render_template("subscribe.html",
                                                 message="Please try harder on the test of humanity below."))

        existing_subscriber = Contact.get_or_none(Contact.address == new_email)
        if existing_subscriber and existing_subscriber.verified:
            return make_response(render_template("subscribe.html",
                                                 message="It appears that you're already on the list."))
        if existing_subscriber and not existing_subscriber.verified:
            email.send_validation(Contact.get(Contact.address == new_email))
            return make_response(render_template("verification.html"))

        # We made it this far so this must be a new subscriber
        new_subscriber = Contact.create(address=new_email)
        logging.warning(f"Created new user: {new_subscriber.token}")
        email.send_validation(new_subscriber)
        return make_response(render_template("verification.html"))

    else:
        # Just need to send a blank form
        return make_response(render_template("subscribe.html"))


@app.route("/conf/<conf_email>/<token>", methods=["GET"])
def confirm(conf_email, token):
    conf_email = urllib.parse.unquote(conf_email)
    subscriber = Contact.select().where(Contact.address == conf_email and Contact.token == token).first()

    if not subscriber:
        logging.warning(f"Couldn't find subscription for {conf_email} with {token}")
        return make_response(render_template("main.html",
                                             message="This confirmation link doesn't seem to be valid."))
    else:
        subscriber.verified = True
        subscriber.save()
        return make_response(render_template("main.html",
                                             message="Congratulations, you are now on the list. Look out for an email"
                                                     " sometime in the indefinite future."))


@app.route("/unsub/<conf_email>/<token>", methods=["GET", "POST"])
def unsubscribe(conf_email, token):
    conf_email = urllib.parse.unquote(conf_email)
    subscriber = Contact.select().where(Contact.address == conf_email and Contact.token == token).first()

    if not subscriber:
        return make_response(render_template("main.html",
                                             message="This unsubscribe link doesn't seem to be valid."))

    if request.method == "GET":
        return make_response(render_template("unsubscribe.html",
                                             email=subscriber.address, token=subscriber.token))

    if request.method == "POST":
        if request.form.get("email") == subscriber.address:
            subscriber.delete_instance()
            return make_response(render_template("unsubscribed.html"))
        else:
            return make_response(render_template("main.html",
                                                 message="This unsubscribe link doesn't seem to be valid."))


@app.errorhandler(404)
def not_found(e):
    return make_response(render_template("main.html",
                                         message="That isn't a valid path, and it's easy to tell because there are"
                                                 " only like three of them."))