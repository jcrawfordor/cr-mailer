from peewee import *
import datetime
import random
import string

database = SqliteDatabase(None)


def get_token():
    return ''.join(random.choices("01234567890abcdefghjklmnpqrstuvwxyzABCDEFGHJKMNPQRSTUVWZYX", k=16))


class BaseModel(Model):
    class Meta:
        database = database


class Contact(BaseModel):
    address = TextField(unique=True, primary_key=True)
    subscribe_time = DateTimeField(default=datetime.datetime.now)
    token = TextField(default=get_token)
    verified = BooleanField(default=False)
