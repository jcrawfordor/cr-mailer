from app import app, email
from app.models import database, Contact
import click
import csv

@app.cli.command("init-db")
def init_db():
    database.create_tables([Contact])
    print(f"Database tables created.")


@app.cli.command("import")
@click.argument("file", required=True)
def import_subs(file):
    count = 0
    with open(file, "r") as f:
        for line in f:
            print(line.strip())
            try:
                Contact.create(address=line.strip(), verified=True)
                count += 1
            except Exception as e:
                print(f"Failed to add subscriber: {e}")
    print(f"Import complete, {count} contacts added.")


@app.cli.command("unsub")
@click.argument("address", required=True)
def manual_unsub(address):
    """Unsubscribe an email"""
    sub = Contact.get_or_none(Contact.address == address)
    if sub:
        sub.delete_instance()
        print(f"Removed subscriber {address}")
    else:
        print(f"Did not find a subscriber with email {address}")


@app.cli.command("confirm")
@click.argument("address", required=True)
def manual_confirm(address):
    """Verify an email w/o user feedback"""
    sub = Contact.get_or_none(Contact.address == address)
    if sub:
        sub.verified = True
        sub.save()
        print(f"Confirmed subscriber {address}")
    else:
        print(f"Did not find a subscriber with email {address}")


@app.cli.command("list")
def list_subs():
    """List all subscribers"""
    contacts = Contact.select()
    for contact in contacts:
        print(f"{contact.address:<36}  {contact.subscribe_time} {contact.verified}")
    print(f"Total count: {contacts.count()}")


@app.cli.command("send")
@click.argument("message", required=True)
def send_all(message):
    contacts = Contact.select().where(Contact.verified == True)
    email.send(message, contacts)

# This is pretty hacky and pretty much a temporary solution
@app.cli.command("bonus")
@click.argument("message", required=True)
@click.argument("subject", required=True)
def send_bonus(message, subject):
    emails = []
    with open('premium.csv', 'r') as f:
        rdr = csv.reader(f)
        next(rdr, None)
        for row in rdr:
            emails.append(row[1])

    email.bonus_message(message, subject, emails)


@app.cli.command("test")
@click.argument("message", required=True)
@click.argument("address", required=True)
def send_test(message, address):
    contacts = Contact.select().where(Contact.address == address)
    if contacts:
        email.send(message, contacts)
    else:
        print(f"Could not find subscriber {address}")
