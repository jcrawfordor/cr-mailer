# cr-mailer

This is the thing that sends the emails for computer.rip. You could probably
use it too if you want. It is a very minimal Flask app with web-based subscribe
nd unsubscribe, and a simple CLI tool that drops text files into a template and
then sends them via SMTP. It's not very fast because none of it is optimized at
all, but then I don't have that many subscribers, so it works out.

Usage:

1. Copy `example-config/config.toml` to an appropriate Flask instance path such
   as `instance/config.toml` at the root of this repo (conveniently where Flask
   will look for it by default). Fill in appropriate values including the path
   where the sqlite database of subscribers will be stored.
2. Use `poetry install` or whatever to get dependencies.
2. Set up some kind of application server stack to run the Flask app, such as 
   uwsgi and nginx.
3. Now run `poetry run flask init-db` to set up the sqlite database.
4. The subscribe interface should now work at /, along with confirmation and
   unsubscribe links.
5. `poetry run flask import` can be used to import a list of subscribers from a
   file that simply contains one email address on each line.
6. `poetry run flask test <message file> <subscriber email>` will send a
   message to the one selected subscriber only, useful for testing.
7. `poetry run flask send <message file>` will send the message to everyone.
8. `poetry run flask unsub <email` can be used to manually unsubscribe someone,
   which is useful for handling bounces because this does not do that
   automatically.

There are values like the URL base and "from" identity that are hard-coded. Sorry.